package library.entities;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import library.entities.ILoan.LoanState;
import library.entities.IBook.BookState;

class LibraryTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testCalculateOverDueFineWhenBookBecomesOverDueByOneDay() {
		//arrange
		int expected = 1;
		Book B = new Book("A","Arjun","T",1);
		Patron P = new Patron("Silwal", "Arjun", "asilwal222@gmail.com",242425, 1);
		Loan loan = new Loan(B,P,new Date(System.currentTimeMillis() - 1 * 24 * 60 * 60 * 1000), LoanState.OVER_DUE, 1);
		Library library = new Library(null, null,null,null,null,null,null,null);
		//Assert
		double actual = library.calculateOverDueFine(loan);
		//Act
		assertEquals(expected, actual);
	}
	
	@Test
	void testCalculateOverDueFineWhenDueDateIsMoreThanOne(){
		//arrange
		int expected = 2;
		Book B = new Book("Ar","Arjun1","TT",2);
		Patron P = new Patron("Sil", "Arjun1", "asilwal@gmail.com",24225, 2);
		Loan loan = new Loan(B,P,new Date(System.currentTimeMillis() - 2 * 24 * 60 * 60 * 1000), LoanState.OVER_DUE, 2);
		Library library = new Library(null, null,null,null,null,null,null,null);
		//Assert
		double actual = library.calculateOverDueFine(loan);
		//Act
		assertEquals(expected, actual);
	}

}
